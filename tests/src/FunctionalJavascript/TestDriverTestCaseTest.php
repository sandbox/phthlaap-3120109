<?php /** @noinspection ALL */

namespace Drupal\Tests\test_case_ui\FunctionalJavascript;

use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Exception\DriverException;
use Drupal;
use weitzman\DrupalTestTraits\ExistingSiteSelenium2DriverTestBase;

/**
 * Class VirtualTestCaseTest.
 *
 * @package Drupal\Tests\test_case_ui\FunctionalJavascript
 */
class TestDriverTestCaseTest extends ExistingSiteSelenium2DriverTestBase implements VirtualTestCaseInterface {

  use Drupal\Tests\test_case_ui\Traits\ScreenShotTestTrait;
  use Drupal\Tests\test_case_ui\Traits\ChoosenSelectOptionTrait;
  use Drupal\Tests\test_case_ui\Traits\GenerateRandomTestTrait;

  protected $runCase;

  /**
   * Drupal\test_case_ui\LogManager.
   *
   * @var \Drupal\test_case_ui\LogManager
   */
  protected $logManager;

  protected $pluginManager;

  protected $entityTypeManager;

  protected $testCaseStorage;
  protected $actionStorage;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->logManager = $this->container->get('test_case_log.manager');
    $this->pluginManager = $this->container->get('plugin.manager.case_action_plugin');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->testCaseStorage = $this->entityTypeManager->getStorage('test_case');
    $this->actionStorage = $this->entityTypeManager->getStorage('test_case_action');

  }

  /**
   * @group runtest
   */
  public function testFirst() {
    $this->assert(TRUE);
    $this->drupalGet('https://lap-trinh.net', ['external' => TRUE]);
    $this->captureScreenshot();
    $session = $this->assertSession();
    dump($this->getCurrentPageContent());
  }

  public function drupalGet($path, array $options = [], array $headers = []) {
    return parent::drupalGet($path, $options, $headers);
  }

  public function randomMachineName($length = 8) {
    return parent::randomMachineName($length);
  }

  public function getRandomGenerator() {
    return parent::getRandomGenerator();
  }
  public function getDriverInstance() {
    if (!isset($this->driver) && ($driverArgs = getenv('DTT_MINK_DRIVER_ARGS') ?: '["firefox", null, "http://localhost:4444/wd/hub"]')) {
      $driverArgs = json_decode($driverArgs, true);
      if (!$driverArgs) {
        throw new DriverException('Invalid driver arguments given for "DTT_MINK_DRIVER_ARGS", make sure to use double quotes inside the brackets.');
      }
      $this->driver = new Selenium2Driver(...$driverArgs);
    }
    return $this->driver;
  }
}


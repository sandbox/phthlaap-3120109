<?php /** @noinspection ALL */

namespace Drupal\Tests\test_case_ui\FunctionalJavascript;

use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Exception\DriverException;
use Drupal;
use weitzman\DrupalTestTraits\ExistingSiteSelenium2DriverTestBase;

/**
 * Class VirtualTestCaseTest.
 *
 * @package Drupal\Tests\test_case_ui\FunctionalJavascript
 */
class VirtualTestCaseTest extends ExistingSiteSelenium2DriverTestBase implements VirtualTestCaseInterface {

  use Drupal\Tests\test_case_ui\Traits\ScreenShotTestTrait;
  use Drupal\Tests\test_case_ui\Traits\ChoosenSelectOptionTrait;
  use Drupal\Tests\test_case_ui\Traits\GenerateRandomTestTrait;

  protected $runCase;

  /**
   * Drupal\test_case_ui\LogManager.
   *
   * @var \Drupal\test_case_ui\LogManager
   */
  protected $logManager;

  protected $pluginManager;

  protected $entityTypeManager;

  protected $testCaseStorage;

  protected $actionStorage;

  protected $webDriverSessionId;

  protected $screenRecordVideo = [];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->logManager = $this->container->get('test_case_log.manager');
    $this->pluginManager = $this->container->get('plugin.manager.case_action_plugin');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->testCaseStorage = $this->entityTypeManager->getStorage('test_case');
    $this->actionStorage = $this->entityTypeManager->getStorage('test_case_action');
  }

  /**
   * @group runtest
   */
  public function testFirst() {
    $case_to_run = getenv('case');
    $test_cases = $this->testCaseStorage->loadByProperties(['id' => $case_to_run]);
    foreach ($test_cases as $test_case) {
      $this->runCase = $test_case;
      $actions = $this->actionStorage->loadByProperties(['test_case' => $test_case->id()]);
      uasort($actions, [
        Drupal\test_case_ui\Entity\TestCaseAction::class,
        'sort',
      ]);
      $this->storeScreenshots = [];
      foreach ($actions as $action) {
        /** @var \Drupal\test_case_ui\Plugin\CaseActionPluginInterface $plugin */
        $plugin = $this->pluginManager->createInstance($action->getCaseAction());
        dump('Process: ' . $action->label());
        $plugin->process($this, $action->getArguments());
      }
    }
    // dump($this->driver->getWebDriverSessionId());
    if (TRUE == getenv('screen_record')) {
      $this->screenRecordVideo[] = getenv('SCREEN_RECORD_VIDEO_URL') . '/' . $this->driver->getWebDriverSessionId() . '.mp4';
    }
  }

  public function drupalGet($path, array $options = [], array $headers = []) {
    return parent::drupalGet($path, $options, $headers);
  }

  public function randomMachineName($length = 8) {
    return parent::randomMachineName($length);
  }

  public function getRandomGenerator() {
    return parent::getRandomGenerator();
  }

  public function tearDown() {
    parent::tearDown();
    $screenshot = array_merge($this->storeScreenshots, $this->screenRecordVideo);
    $this->logManager->write($this->runCase, $this->getStatusMessage(), $screenshot);
  }


  public function getDriverInstance() {
    if (!isset($this->driver) && ($driverArgs = getenv('DTT_MINK_DRIVER_ARGS') ?: '["firefox", null, "http://localhost:4444/wd/hub"]')) {
      $driverArgs = json_decode($driverArgs, true);
      if (TRUE == getenv('screen_record')) {
        $driverArgs[1]['enableVideo'] = TRUE;
        $driverArgs[1]['videoScreenSize'] = '1024x768';
      }
      if (!$driverArgs) {
        throw new DriverException('Invalid driver arguments given for "DTT_MINK_DRIVER_ARGS", make sure to use double quotes inside the brackets.');
      }
      $this->driver = new Selenium2Driver(...$driverArgs);

    }
    return $this->driver;
  }
}


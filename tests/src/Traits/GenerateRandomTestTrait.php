<?php

namespace Drupal\Tests\test_case_ui\Traits;

/**
 * Trait GenerateRandomTestTrait
 *
 * @package Drupal\Tests\test_case_ui\Traits
 */
trait GenerateRandomTestTrait {

  /**
   * Random number.
   *
   * @param int $length
   *
   * @return string
   */
  public function randomNumber($length = 8) {
    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $str .= chr(mt_rand(48, 57));
    }

    return $str;
  }

}

<?php

namespace Drupal\Tests\test_case_ui\Traits;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Cookie\CookieJar;

/**
 * Trait RequestHelperTrait.
 *
 * @package Drupal\Tests\simple_oauth\Functional
 */
trait RequestHelperTrait {

  public $cookieJar = NULL;

  public $useCookie = FALSE;

  public $client = NULL;

  public function getHttpClient() {
    if (!empty($this->client)) {
      return $this->client;
    }
    $this->client = new Client(['cookies' => TRUE]);
    return $this->client;
  }

  /**
   * POST a request.
   *
   * The base methods do not provide a non-form submission POST method.
   *
   * @param \Drupal\Core\Url $url
   *   The URL.
   * @param array $data
   *   The data to send.
   * @param array $options
   *   Optional options to pass to client.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://www.drupal.org/project/drupal/issues/2908589#comment-12258839
   */
  protected function post(Url $url, array $data, array $options = []) {
    $post_url = $this->getAbsoluteUrl($url->toString());
    $post_option = [
      'form_params' => $data,
      'http_errors' => FALSE,
    ];
    if ($this->useCookie == TRUE) {
      $this->initCookieJar();
      $post_option['cookies'] = $this->cookieJar;
    }
    $response = $this->getHttpClient()->request('POST', $post_url, $post_option + $options);
    // dump($response->get());
//    dump($post_option['cookies']);
    return $response;
  }

  /**
   * Init Cookie.
   *
   * @return \GuzzleHttp\Cookie\CookieJar
   *   Cookie.
   */
  public function initCookieJar() {
    if (!empty($this->cookieJar)) {
      return $this->cookieJar;
    }
    $this->cookieJar = new CookieJar();
    return $this->cookieJar;
  }

  /**
   * GET a resource, with options.
   *
   * @param \Drupal\Core\Url $url
   *   Url.
   * @param array $options
   *   Option.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  protected function get(Url $url, array $options = []) {
    $options += [
      RequestOptions::HTTP_ERRORS => FALSE,
    ];
    $get_url = $this->getAbsoluteUrl($url->toString());
    return $this->getHttpClient()->get($get_url, $options);
  }

  protected function delete(Url $url, $options = []) {
    $options += [
      RequestOptions::HTTP_ERRORS => FALSE,
    ];
    $get_url = $this->getAbsoluteUrl($url->toString());
    //$session->setCookie('SIMPLETEST_USER_AGENT', drupal_generate_test_ua($this->databasePrefix));
    return $this->getHttpClient()->delete($get_url, $options);
  }

  protected function getAccessToken($post) {
    /*$post = [
      'grant_type' => 'password',
      'client_id' => '5d22b0a2-7722-48d6-9f22-9e359c0d1dfa',
      'client_secret' => '123456',
      'username' => $this->leaderAccount->getUsername(),
      'password' => '123456',
    ];*/
    $response = $this->post(Url::fromUri('internal:/oauth/token', ['absolute' => TRUE]), $post);
    $json_response = Json::decode((string) $response->getBody());
    return $json_response;
  }

  /**
   * Post with json body array.
   *
   * @param array $body
   *   Array of body.
   * @param string $url
   *   Url.
   * @param array $headers
   *   Header.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Json return.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function doPost(array $body, $url, array $headers = NULL) {
    if ($headers == NULL) {
      $headers = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ];
    }
    $post_data = [
      'headers' => $headers,
      'http_errors' => FALSE,
    ];
    if (!empty($body)) {
      $post_data['body'] = Json::encode($body);
    }
    if ($this->useCookie == TRUE) {
      $this->initCookieJar();
      $post_data['cookies'] = $this->cookieJar;
    }
    $response = $this->getHttpClient()->request('POST', $url->toString(), $post_data);
    return $response;
  }

}

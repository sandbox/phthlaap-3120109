<?php


namespace Drupal\Tests\test_case_ui\Traits;

/**
 * Trait ChoosenSelectOptionTrait.
 *
 * @package Drupal\Tests\chms_automated_tests\Traits
 */
trait ChoosenSelectOptionTrait {

  /**
   * Select a option of choonsen.
   */
  public function selectChoosenOption($wrapper, $option) {
    $this->assertSession()->waitForElementVisible('css', $wrapper);
    $this->assertSession()
      ->elementExists('css', $wrapper)
      ->click();

    $assert = $this->assertSession();
    $assert->waitForElement('css', $wrapper . " " . $option)->click();
  }

  /**
   * Select a checkbox.
   */
  public function checkboxCheckByLabel($selector, $index = 0) {
    $options = $this->getSession()
      ->getPage()
      ->findAll('css', $selector);
    $options[$index]->click();
  }

}

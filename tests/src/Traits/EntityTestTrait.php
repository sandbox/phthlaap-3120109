<?php

namespace Drupal\Tests\test_case_ui\Traits;

use Drupal\Core\Entity\EntityInterface;

/**
 * Trait EntityTestTrait.
 *
 * @package Drupal\Tests\chms_automated_tests\Traits
 */
trait EntityTestTrait {

  protected $markedCleanEntity = TRUE;

  /**
   * Reload an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to reload.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Reloaded entity.
   */
  protected function reloadEntity(EntityInterface $entity) {
    $controller = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $controller->resetCache([$entity->id(),]);
    return $controller->load($entity->id());
  }

}

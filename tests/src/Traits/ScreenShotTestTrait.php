<?php

namespace Drupal\Tests\test_case_ui\Traits;

use Drupal\Component\Utility\Html;

/**
 * Trait ScreenShotTestTrait.
 *
 * @package Drupal\Tests\test_case_ui\Traits
 */
trait ScreenShotTestTrait {

  public $storeScreenshots = [];

  /**
   * Captures and saves a screenshots.
   *
   * The filename generated screenshots will contain a unique ID, the URL where
   * the screenshots was taken and the given base filename.
   *
   * @param string $base_filename
   *   The base filename to use, defaults to 'screenshots'.
   * @param string $specific
   *   Specific directory.
   */
  public function captureScreenshot($base_filename = 'screenshot', $specific = NULL) {
    $directory = getenv('DTT_SCREENSHOT_REPORT_DIRECTORY') ?: '/sites/simpletest/browser_output';
    if (!empty($specific)) {
      $directory .= '/' . $specific;
    }
    if ($directory) {
      // Ensure directory exists.
      if (!is_dir($directory)) {
        mkdir($directory, 0777, TRUE);
      }
      $current_url = Html::cleanCssIdentifier($this->getSession()
        ->getCurrentUrl());
      $base_filename = empty($base_filename) ? Html::cleanCssIdentifier($base_filename) . '_' : '';
      $filename = file_create_filename($base_filename . date('his-dMY') . '_' . '.png', $directory);
      $screenshot = $this->getDriverInstance()->getScreenshot();
      file_put_contents($filename, $screenshot);
      $this->storeScreenshots[] = $filename;
    }
  }

}

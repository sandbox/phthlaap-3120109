<?php

namespace Drupal\test_case_ui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Test case assert entity.
 *
 * @ConfigEntityType(
 *   id = "test_case_assert",
 *   label = @Translation("Test case assert"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\test_case_ui\TestCaseAssertListBuilder",
 *     "form" = {
 *       "add" = "Drupal\test_case_ui\Form\TestCaseAssertForm",
 *       "edit" = "Drupal\test_case_ui\Form\TestCaseAssertForm",
 *       "delete" = "Drupal\test_case_ui\Form\TestCaseAssertDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\test_case_ui\TestCaseAssertHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "test_case_assert",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/test_case_assert/{test_case_assert}",
 *     "add-form" = "/admin/structure/test_case_assert/add",
 *     "edit-form" = "/admin/structure/test_case_assert/{test_case_assert}/edit",
 *     "delete-form" = "/admin/structure/test_case_assert/{test_case_assert}/delete",
 *     "collection" = "/admin/structure/test_case_assert"
 *   }
 * )
 */
class TestCaseAssert extends ConfigEntityBase implements TestCaseAssertInterface {

  /**
   * The Test case assert ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Test case assert label.
   *
   * @var string
   */
  protected $label;

}

<?php

namespace Drupal\test_case_ui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Test case assert entities.
 */
interface TestCaseAssertInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

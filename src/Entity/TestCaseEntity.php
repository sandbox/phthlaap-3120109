<?php

namespace Drupal\test_case_ui\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Test case entity entity.
 *
 * @ConfigEntityType(
 *   id = "test_case",
 *   label = @Translation("Test case entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\test_case_ui\TestCaseEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\test_case_ui\Form\TestCaseEntityForm",
 *       "edit" = "Drupal\test_case_ui\Form\TestCaseEntityForm",
 *       "delete" = "Drupal\test_case_ui\Form\TestCaseEntityDeleteForm",
 *       "clone" = "Drupal\test_case_ui\Form\TestCaseEntityCloneForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\test_case_ui\TestCaseEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "test_case",
 *   admin_permission = "administer test case ui",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/test_case/{test_case}",
 *     "add-form" = "/admin/structure/test_case/add",
 *     "edit-form" = "/admin/structure/test_case/{test_case}/edit",
 *     "delete-form" = "/admin/structure/test_case/{test_case}/delete",
 *     "clone" = "/admin/structure/test_case/{test_case}/clone",
 *     "collection" = "/admin/structure/test_case"
 *   }
 * )
 */
class TestCaseEntity extends ConfigEntityBase implements TestCaseEntityInterface {

  /**
   * The Test case entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Test case entity label.
   *
   * @var string
   */
  protected $label;

  protected $log_message;

  protected $log_images;

  protected $uid;

  protected $remarks;

  protected $screen_record;

  /**
   * Get log images.
   *
   * @return array
   *   Return array of images.
   */
  public function getLogImages() {
    if (empty($this->log_images)) {
      return [];
    }
    return Json::decode($this->log_images);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if ($this->isNew()) {
      $this->uid = \Drupal::currentUser()->id();
    }
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * Get owner of entity.
   *
   * @return \Drupal\user\UserInterface|null
   *   Drupal\user\UserInterface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOwner() {
    if (empty($this->uid)) {
      return NULL;
    }
    $user_storage = $this->entityTypeManager()->getStorage('user');
    return $user_storage->load($this->uid);
  }

  /**
   * Get remarks.
   *
   * @return mixed
   *   String.
   */
  public function getRemarks() {
    return $this->remarks;
  }

  /**
   * Get screen record.
   *
   * @return bool
   *   Bool.
   */
  public function getScreenRecord() {
    return $this->screen_record;
  }

}

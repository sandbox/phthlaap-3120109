<?php

namespace Drupal\test_case_ui\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Test case action entity.
 *
 * @ConfigEntityType(
 *   id = "test_case_action",
 *   label = @Translation("Test case action"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\test_case_ui\TestCaseActionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\test_case_ui\Form\TestCaseActionForm",
 *       "edit" = "Drupal\test_case_ui\Form\TestCaseActionForm",
 *       "delete" = "Drupal\test_case_ui\Form\TestCaseActionDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\test_case_ui\TestCaseActionHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "test_case_action",
 *   admin_permission = "administer test case ui",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/test_case_action/{test_case_action}",
 *     "add-form" = "/admin/structure/test_case_action/add",
 *     "edit-form" = "/admin/structure/test_case_action/{test_case_action}/edit",
 *     "delete-form" = "/admin/structure/test_case_action/{test_case_action}/delete",
 *     "collection" = "/admin/structure/test_case_action"
 *   }
 * )
 */
class TestCaseAction extends ConfigEntityBase implements TestCaseActionInterface {

  /**
   * The Test case action ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Test case action label.
   *
   * @var string
   */
  protected $label;

  protected $test_case;

  protected $case_action;

  protected $arguments;

  protected $weight;

  protected $uid;

  /**
   * Get arguments.
   *
   * @return mixed
   *   Arguments.
   */
  public function getArguments() {
    return Json::decode($this->arguments);
  }

  /**
   * Get case action.
   *
   * @return mixed
   *   Case action.
   */
  public function getCaseAction() {
    return $this->case_action;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if ($this->isNew()) {
      $this->uid = \Drupal::currentUser()->id();
    }
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * Get owner of entity.
   *
   * @return \Drupal\user\UserInterface|null
   *   Drupal\user\UserInterface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOwner() {
    if (empty($this->uid)) {
      return NULL;
    }
    $user_storage = $this->entityTypeManager()->getStorage('user');
    return $user_storage->load($this->uid);
  }

}

<?php

namespace Drupal\test_case_ui\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Case action plugin item annotation object.
 *
 * @see \Drupal\test_case_ui\Plugin\CaseActionPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class CaseActionPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}

<?php

namespace Drupal\test_case_ui;

use Drupal\Component\Datetime\Time;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\test_case_ui\Entity\TestCaseEntity;

/**
 * Class TestCaseManager.
 *
 * @package Drupal\test_case_ui
 */
class LogManager {

  protected $entityTypeManager;

  protected $database;

  public static $table = 'test_case_log';

  /**
   * Drupal\Component\Datetime\Time.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Connection $connection,
    Time $time
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $connection;
    $this->time = $time;
  }

  /**
   * Write log to database table.
   *
   * @param \Drupal\test_case_ui\Entity\TestCaseEntity $entity
   *   Drupal\test_case_ui\Entity\TestCaseEntity.
   * @param string $message
   *   Message.
   * @param array $images
   *   Array of images.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|null
   *   Return.
   *
   * @throws \Exception
   */
  public function write(TestCaseEntity $entity, $message = '', array $images = []) {
    $datetime = DrupalDateTime::createFromTimestamp($this->time->getCurrentTime(), date_default_timezone_get());
    $data = [
      'test_case' => $entity->id(),
      'message' => $message,
      'images' => Json::encode($images),
      'created' => $datetime->format('Y-m-d\TH:i:s'),
    ];
    try {
      $result = $this->database->insert(self::$table)->fields($data)->execute();
    }
    catch (\Exception $exception) {
      throw $exception;
    }
    return $result;
  }

  /**
   * Get latest log.
   *
   * @param \Drupal\test_case_ui\Entity\TestCaseEntity $entity
   *
   * @return
   */
  public function latest(TestCaseEntity $entity) {
    $table = self::$table;
    $query = $this->database->query("SELECT * FROM {$table} WHERE test_case = :case ORDER BY created DESC LIMIT 0, 1", ['case' => $entity->id()]);
    $query->setFetchMode(\PDO::FETCH_CLASS, TestCaseLog::class);
    return $query->fetch(\PDO::FETCH_CLASS);
  }

}

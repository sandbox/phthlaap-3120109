<?php

namespace Drupal\test_case_ui;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Test case action entities.
 */
class TestCaseActionListBuilder extends ConfigEntityListBuilder {

  protected $currentUser;

  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    AccountInterface $currentUser,
    PluginManagerInterface $pluginManager
  ) {
    parent::__construct($entity_type, $storage);
    $this->currentUser = $currentUser;
    $this->pluginManager = $pluginManager;

  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_user'),
      $container->get('plugin.manager.case_action_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityIds() {
    $query = $this->getStorage()->getQuery();
    if (!$this->currentUser->hasPermission('administer test case ui configuration')) {
      $filter_owner = $query->orConditionGroup();
      $filter_owner->condition('uid', [\Drupal::currentUser()->id(), 0], 'IN');
      $filter_owner->condition('uid', NULL, 'IS NULL');
      $query->condition($filter_owner);
    }
    $query->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['weight'] = $this->t('#');
    $header['label'] = $this->t('Test case action');
    $header['id'] = $this->t('Machine name');
    $header['arguments'] = $this->t('Arguments');
    $header['uid'] = $this->t('Owner');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['weight'] = $entity->get('weight');
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();

    /** @var \Drupal\test_case_ui\Plugin\CaseActionPluginInterface $plugin */
    $plugin = $this->pluginManager->createInstance($entity->getCaseAction());
    $definition = $plugin->getPluginDefinition();
    $arguments = $entity->getArguments();
    $arguments = $plugin->preProcessArguments($arguments);
    $arguments_output = '';
    if (!empty($arguments)) {
      $arguments_output = implode('<br/>', $arguments);
    }
//    dump($arguments_output);
    $row['arguments']['data'] = [
      '#markup' => $arguments_output,
    ];
    /** @var \Drupal\user\UserInterface $owner */
    $owner = $entity->getOwner();
    $row['uid'] = $owner ? $owner->toLink() : '';
    return $row + parent::buildRow($entity);
  }

}

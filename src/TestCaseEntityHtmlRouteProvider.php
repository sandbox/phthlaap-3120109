<?php

namespace Drupal\test_case_ui;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Test case entity entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class TestCaseEntityHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($clone_route = $this->getCloneRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.clone", $clone_route);
    }
    return $collection;
  }

  /**
   * Get clone route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Drupal\Core\Entity\EntityTypeInterface.
   *
   * @return \Symfony\Component\Routing\Route
   *   Symfony\Component\Routing\Route.
   */
  protected function getCloneRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('clone')) {
      $route = new Route($entity_type->getLinkTemplate('clone'));
      $route
        ->setDefaults([
          '_title' => "{$entity_type->getLabel()} clone",
          '_entity_form' => 'test_case.clone',
        ])
        ->setRequirement('_permission', 'administer test case ui')
        ->setOption('_admin_route', TRUE)
        ->setOptions(['parameters' => ['test_case' => ['type' => 'entity:test_case']]]);

      return $route;
    }
  }

}

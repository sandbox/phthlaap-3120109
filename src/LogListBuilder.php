<?php

namespace Drupal\test_case_ui;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\test_case_ui\Entity\TestCaseEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LogListBuilder.
 *
 * @package Drupal\test_case_ui
 */
class LogListBuilder implements ContainerInjectionInterface {

  protected $connection;

  protected $limit = 30;

  /**
   * Drupal\test_case_ui\TestCaseLog.
   *
   * @var \Drupal\test_case_ui\Entity\TestCaseEntity
   */
  protected $testCase;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, TestCaseEntity $testCase) {
    $this->connection = $connection;
    $this->testCase = $testCase;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, TestCaseEntity $testCase = NULL) {
    return new static(
      $container->get('database'),
      $testCase
    );
  }

  /**
   * Render log table.
   *
   * @return mixed
   *   Return render table.
   */
  public function render() {
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
    ];
    /** @var \Drupal\test_case_ui\TestCaseLog $entity */
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->getId()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

  /**
   * Build header of table.
   *
   * @return array
   *   Array of header.
   */
  private function buildHeader() {
    return [
      //['data' => 'ID'],
      ['data' => 'Created'],
      ['data' => 'Message'],
    ];
  }

  /**
   * Get title.
   */
  private function getTitle() {
    return;
  }

  /**
   * Load data for table.
   *
   * @return array
   *   Result.
   */
  private function load() {
    $table = LogManager::$table;
    $query = $this->connection->select($table, $table);
    $query->fields($table, [
      'lid',
      'created',
      'message',
      'images',
      'test_case',
    ]);
    if (!empty($this->testCase)) {
      $query->condition('test_case', $this->testCase->id());
    }
    $query->orderBy('lid', 'DESC');
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit($this->limit);
    $result = $pager->execute();
    $result->setFetchMode(\PDO::FETCH_CLASS, TestCaseLog::class);
    return $result->fetchAll();
  }

  /**
   * Build table row.
   *
   * @param \Drupal\test_case_ui\TestCaseLog $entity
   *   Drupal\test_case_ui\TestCaseLog.
   *
   * @return array
   *   Row result.
   */
  private function buildRow(TestCaseLog $entity) {
    return [
      'data' => [
        // 'id' => $entity->getId(),
        'created' => $entity->getCreated(),
        'message' => $entity->getMessage(),
      ],
    ];
  }

}

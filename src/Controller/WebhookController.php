<?php

namespace Drupal\test_case_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WebhookController.
 */
class WebhookController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\test_case_ui\TestCaseManager definition.
   *
   * @var \Drupal\test_case_ui\TestCaseManager
   */
  protected $testCaseManager;

  /**
   * Drupal\test_case_ui\LogManager definition.
   *
   * @var \Drupal\test_case_ui\LogManager
   */
  protected $testCaseLogManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->testCaseManager = $container->get('test_case.manager');
    $instance->testCaseLogManager = $container->get('test_case_log.manager');
    return $instance;
  }

  /**
   * Hello.
   *
   * @param \Drupal\Core\Entity\EntityInterface $test_case
   *   Drupal\Core\Entity\EntityInterface.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return Hello string.
   */
  public function runTest(EntityInterface $test_case, Request $request) {
    $output = [];
    $case_id = $test_case->id();
    $script = "cd " . DRUPAL_ROOT . " && case={$case_id} ../vendor/bin/phpunit -c ./phpunit.xml --filter testFirst modules/custom/test_case_ui/tests/src/FunctionalJavascript/VirtualTestCaseTest.php";
    exec($script . ' > /dev/null &');
    \Drupal::logger('test_case_ui')->debug(implode('<br/>', $output));
    return new JsonResponse(['data' => [], 'status' => 1]);
  }

}

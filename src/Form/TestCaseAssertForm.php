<?php

namespace Drupal\test_case_ui\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TestCaseAssertForm.
 */
class TestCaseAssertForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $test_case_assert = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $test_case_assert->label(),
      '#description' => $this->t("Label for the Test case assert."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $test_case_assert->id(),
      '#machine_name' => [
        'exists' => '\Drupal\test_case_ui\Entity\TestCaseAssert::load',
      ],
      '#disabled' => !$test_case_assert->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $test_case_assert = $this->entity;
    $status = $test_case_assert->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Test case assert.', [
          '%label' => $test_case_assert->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Test case assert.', [
          '%label' => $test_case_assert->label(),
        ]));
    }
    $form_state->setRedirectUrl($test_case_assert->toUrl('collection'));
  }

}

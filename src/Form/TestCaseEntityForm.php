<?php

namespace Drupal\test_case_ui\Form;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\test_case_ui\ActionCaseDetailListBuilder;
use Drupal\test_case_ui\LogListBuilder;
use Drupal\test_case_ui\LogManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestCaseEntityForm.
 */
class TestCaseEntityForm extends EntityForm {

  protected $listBuilder;

  protected $entityTypeManager;

  protected $container;

  protected $logManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Container $container,
    EntityTypeManagerInterface $entityTypeManager,
    LogManager $logManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->container = $container;
    $this->logManager = $logManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('entity_type.manager'),
      $container->get('test_case_log.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntity() {
    parent::prepareEntity();

    $storage = $this->entityTypeManager->getStorage('test_case_action');
    $entity_type = $storage->getEntityType();
    $this->listBuilder = ActionCaseDetailListBuilder::createInstance($this->container, $entity_type, ['test_case' => $this->entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $test_case = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $test_case->label(),
      '#description' => $this->t("Label for the Test case entity."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $test_case->id(),
      '#machine_name' => [
        'exists' => '\Drupal\test_case_ui\Entity\TestCaseEntity::load',
      ],
      '#disabled' => !$test_case->isNew(),
    ];
    if (!$this->entity->isNew()) {
      $form['test_button'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['container-inline', 'form-item'],
        ],
      ];
      $form['test_button']['run_options'] = [
        '#type' => 'details',
        '#title' => 'Run options',
        '#open' => FALSE,
      ];
      $form['test_button']['run_options']['screen_record'] = [
        '#type' => 'checkbox',
        '#title' => 'Screen record',
        // '#suffix' => '<em><b>*Please note screen record files will publish, all member can see them.</b></em>',
        '#default_value' => $this->entity->getScreenRecord(),
      ];
      $form['test_button']['run'] = [
        '#type' => 'submit',
        '#value' => 'Run',
        '#ajax' => [
          'callback' => '::runScript',
          'method' => 'replace',
          'wrapper' => 'test-run-output',
        ],
      ];
      $form['test_button']['view_log'] = [
        '#type' => 'submit',
        '#value' => 'View log',
        '#ajax' => [
          'callback' => '::viewLog',
          'event' => 'click',
        ],
      ];
      $form['test_output'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'test-run-output',
        ],
        'images' => [
          '#prefix' => '<br/>',
          '#markup' => implode('<br/>', $this->getImageList()),
        ],
      ];
      /** @var \Drupal\test_case_ui\TestCaseLog $log */
      $log = $this->logManager->latest($this->entity);
      //dump(!empty($log));
      if (!empty($log)) {
        $form['test_output']['message'] = [
          '#prefix' => '<br/>',
          '#markup' => 'Last run ' . $log->getCreated() . '<br/>' . ($log->getMessage() ? $log->getMessage() : ''),
        ];
      }
      $form['case_actions_list_wrapper'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => 'Actions',
      ];
      $form['case_actions_list_wrapper']['add_new_action'] = [
        '#type' => 'submit',
        '#value' => 'Add Action',
        '#submit' => ['::addNewAction'],
      ];
      $element = [];
      $form['case_actions_list_wrapper']['case_actions_list'] = $this->listBuilder->render($element, $form_state);
      $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    }
    $form['remarks_wrapper'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => 'Remarks',
    ];
    $form['remarks_wrapper']['remarks'] = [
      '#type' => 'text_format',
      '#title' => 'Please type anything you want to note.',
      '#format' => 'basic_html',
      '#name' => 'remarks',
      '#default_value' => $test_case->getRemarks(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    $element = parent::afterBuild($element, $form_state);
    // dump($element);
    if (!$this->entity->isNew()) {
      $element['actions']['cancel'] = [
        '#type' => 'link',
        '#title' => 'Cancel',
        '#url' => $this->entity->toUrl('collection'),
        '#weight' => 99,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $test_case = $this->entity;
    if (!$this->entity->isNew()) {
      $this->listBuilder->submitForm($form, $form_state);
    }
    $test_case->set('remarks', $form_state->getValue(['remarks', 'value']));
    $status = $test_case->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Test case entity.', [
            '%label' => $test_case->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Test case entity.', [
            '%label' => $test_case->label(),
          ]));
    }
    if (SAVED_NEW == $status) {
      $form_state->setRedirectUrl($test_case->toUrl('collection'));
    }
  }

  /**
   * Redirect to add action form with destination parameter.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal\Core\Form\FormStateInterface.
   */
  public function addNewAction(array &$form, FormStateInterface $form_state) {
    $redirect = Url::fromRoute('entity.test_case.edit_form', ['test_case' => $this->entity->id()]);
    $parameters = [
      'destination' => $redirect->toString(),
      'test_case' => $this->entity->id(),
    ];
    $response = $this->redirect('entity.test_case_action.add_form', $parameters);
    $response->send();
  }

  /**
   * Test one case.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Form output.
   */
  public function runScript(array &$form, FormStateInterface $form_state) {
    $output = [];
    $case_id = $this->entity->id();
    $evn = [];
    $evn[] = "case={$case_id}";
    if (TRUE == $this->entity->getScreenRecord()) {
      $evn[] = "screen_record=TRUE";
    }
    $script = implode(' ', $evn) . " ../vendor/bin/phpunit -c ./phpunit.xml --filter testFirst modules/custom/test_case_ui/tests/src/FunctionalJavascript/VirtualTestCaseTest.php";
    exec("cd " . DRUPAL_ROOT . " && " . $script . " 2>&1", $output);
    $form['test_output'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'test-run-output',
      ],
      'output' => [
        '#markup' => implode('<br/>', $output),
      ],
      'images' => [
        '#prefix' => '<br/>',
        '#markup' => implode('<br/>', $this->getImageList()),
      ],
    ];

    //$output[] = implode('<br/>', $this->getImageList());
    return $form['test_output'];
  }

  /**
   * Get images list.
   *
   * @return array
   *   Array of images.
   */
  public function getImageList() {
    $entity = $this->entity;
    if ($entity->isNew()) {
      return [];
    }

    /** @var \Drupal\test_case_ui\TestCaseLog $log */
    $log = $this->logManager->latest($this->entity);
    if (empty($log)) {
      return [];
    }
    $images = $log->getImages();
    $images_list = [];
    // $front = Url::fromRoute('<front>');
    foreach ($images as $image) {
      $url = str_replace('./', '/', $image);
      $images_list[] = "<a target='_blank' href='{$url}'>{$url}</a>";
    }
    return $images_list;
  }

  /**
   * View all log of a case.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax response.
   */
  public function viewLog(array $form, FormStateInterface $formState) {
    $response = new AjaxResponse();
    $logList = LogListBuilder::create($this->container, $this->entity);
    $response->addCommand(new OpenModalDialogCommand(t('Logs'), $logList->render(), ['width' => '700']));
    return $response;
  }

}

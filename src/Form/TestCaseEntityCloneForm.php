<?php

namespace Drupal\test_case_ui\Form;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\test_case_ui\ActionCaseDetailListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestCaseEntityCloneForm.
 */
class TestCaseEntityCloneForm extends EntityForm {

  protected $listBuilder;

  protected $entityTypeManager;

  protected $container;

  protected $actionStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(Container $container, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->container = $container;
    $this->actionStorage = $this->entityTypeManager->getStorage('test_case_action');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $test_case = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => 'Clone of ' . $test_case->label(),
      '#description' => $this->t("Label for the Test case entity."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => 'clone_of_' . $test_case->id(),
      '#machine_name' => [
        'exists' => '\Drupal\test_case_ui\Entity\TestCaseEntity::load',
      ],
      //'#disabled' => !$test_case->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    $element = parent::afterBuild($element, $form_state);
    // dump($element);
    if (!$this->entity->isNew()) {
      $element['actions']['cancel'] = [
        '#type' => 'link',
        '#title' => 'Cancel',
        '#url' => $this->entity->toUrl('collection'),
        '#weight' => 99,
      ];
    }
    unset($element['actions']['delete']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {

      $clone_entity = $this->entity->createDuplicate();
      $this->copyFormValuesToEntity($clone_entity, $form, $form_state);
      $clone_entity->save();
      $this->cloneTestCaseAction($clone_entity, $this->entity->getOriginalId());
    }
    catch (\Exception $e) {
      throw $e;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

  }

  /**
   * Clone test case action.
   *
   * @param \Drupal\Core\Entity\EntityInterface $clone_entity
   *   Entity destination.
   * @param string $original_id
   *   Original entity source id.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cloneTestCaseAction(EntityInterface $clone_entity, string $original_id) {
    $actions = $this->actionStorage->loadByProperties(['test_case' => $original_id]);
    $time = \Drupal::time()->getCurrentTime();
    $i = 0;
    foreach ($actions as $action) {
      /** @var \Drupal\Core\Entity\EntityInterface $clone_action */
      $clone_action = $action->createDuplicate();
      $clone_action->set('id', $clone_entity->id() . '_' . $time . '_' . $i);
      $clone_action->set('test_case', $clone_entity->id());
      $clone_action->save();
      $i++;
    }
  }

}

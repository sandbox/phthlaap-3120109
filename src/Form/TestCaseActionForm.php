<?php

namespace Drupal\test_case_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\test_case_ui\Plugin\CaseActionPluginManager;
use Drupal\test_case_ui\TestCaseManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class TestCaseActionForm.
 */
class TestCaseActionForm extends EntityForm {

  protected $actionPluginManager;

  protected $testCaseManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(CaseActionPluginManager $actionPluginManager, TestCaseManager $testCaseManager) {
    $this->actionPluginManager = $actionPluginManager;
    $this->testCaseManager = $testCaseManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.case_action_plugin'),
      $container->get('test_case.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    // dump(\Drupal::request()->get('test_case'));
    $test_case_action = $this->entity;
    $test_case_from_param = \Drupal::request()->get('test_case');

    $form['#id'] = 'test-case-action-form';
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $test_case_action->label(),
      '#description' => $this->t("Label for the Test case action."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $test_case_action->id(),
      '#machine_name' => [
        'exists' => '\Drupal\test_case_ui\Entity\TestCaseAction::load',
      ],
      '#disabled' => !$test_case_action->isNew(),
    ];
    if ($test_case_action->isNew()) {
      $form['id']['#default_value'] = $test_case_from_param . '_' . time();
    }
    $form['weight'] = [
      '#type' => 'number',
      '#title' => 'Weight',
      '#default_value' => $test_case_action->get('weight') ? $test_case_action->get('weight') : 100,
    ];
    $form['test_case'] = [
      '#type' => 'select',
      '#options' => $this->testCaseManager->testCaseOptions(),
      // '#disabled' => TRUE,
      '#title' => 'Test case ID',
      '#default_value' => $test_case_action->get('test_case'),
    ];
    if ($this->entity->isNew() && !empty($test_case_from_param)) {
      $form['test_case']['#default_value'] = $test_case_from_param;
    }

    $options = $this->actionPluginManager->getPluginSelectOptions();
    $form['case_action'] = [
      '#type' => 'select',
      '#title' => 'Action',
      '#required' => TRUE,
      '#options' => $options,
      '#ajax' => [
        'callback' => '::caseActionChange',
        'method' => 'replace',
        'wrapper' => $form['#id'],
      ],
      '#default_value' => $test_case_action->get('case_action'),
    ];
    $this->buildActionElement($test_case_action->get('case_action'), $form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    $element = parent::afterBuild($element, $form_state);
    // dump($element);
    $element['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => 'Cancel',
      '#url' => Url::fromUri('internal:' . \Drupal::request()
          ->get('destination')),
      '#weight' => 99,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityInterface $test_case_action */
    $test_case_action = $this->entity;

    $input = $form_state->getUserInput();
    if (!empty($input['args'])) {
      $args = Json::encode($input['args']);
      $test_case_action->set('arguments', $args);
    }


    $status = $test_case_action->save();
    //    dump($form_state->getUserInput());
    //    dump($form_state->getValue('args'));
    //    die();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Test case action.', [
            '%label' => $test_case_action->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Test case action.', [
            '%label' => $test_case_action->label(),
          ]));
    }
    $form_state->setRedirectUrl($test_case_action->toUrl('collection'));
  }

  /**
   * Case action select change.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal\Core\Form\FormStateInterface.
   *
   * @return array
   *   Return form.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function caseActionChange(array &$form, FormStateInterface $form_state) {
    $case_action = $form_state->getValue('case_action');
    if (empty($case_action)) {
      return $form;
    }
    $this->buildActionElement($case_action, $form, $form_state);
    return $form;
  }

  /**
   * Build action element.
   *
   * @param string $case_action
   *   Case action.
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal\Core\Form\FormStateInterface.
   *
   * @return mixed
   *   Element.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function buildActionElement($case_action, array &$form, FormStateInterface $form_state) {
    if (empty($case_action)) {
      return $form;
    }
    /** @var \Drupal\test_case_ui\Plugin\CaseActionPluginInterface $plugin */
    $plugin = $this->actionPluginManager->createInstance($case_action);
    if (empty($plugin)) {
      return $form;
    }
    $plugin->buildFormElement($form, $form_state, $this->entity->getArguments());
  }

}

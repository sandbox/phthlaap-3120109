<?php

namespace Drupal\test_case_ui\Form;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete Test case entity entities.
 */
class TestCaseEntityDeleteForm extends EntityConfirmFormBase {

  protected $entityTypeManager;

  protected $container;

  protected $actionStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(Container $container, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->container = $container;
    $this->actionStorage = $this->entityTypeManager->getStorage('test_case_action');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.test_case.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $case_id = $this->entity->id();
      $this->entity->delete();
      $case_actions = $this->actionStorage->loadByProperties(['test_case' => $case_id]);
      if (!empty($case_actions)) {
        $this->actionStorage->delete(array_values($case_actions));
      }
    }
    catch (\Exception $exception) {
      throw $exception;
    }

    $this->messenger()->addMessage(
      $this->t('content @type: deleted @label.', [
        '@type' => $this->entity->bundle(),
        '@label' => $this->entity->label(),
      ])
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}

<?php

namespace Drupal\test_case_ui;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Test case entity entities.
 */
class TestCaseEntityListBuilder extends ConfigEntityListBuilder {

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    AccountInterface $currentUser
  ) {
    parent::__construct($entity_type, $storage);
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityIds() {
    $query = $this->getStorage()->getQuery();
    if (!$this->currentUser->hasPermission('administer test case ui configuration')) {
      $filter_owner = $query->orConditionGroup();
      $filter_owner->condition('uid', [\Drupal::currentUser()->id(), 0], 'IN');
      $filter_owner->condition('uid', NULL, 'IS NULL');
      $query->condition($filter_owner);
    }
    $query->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Test case entity');
    $header['id'] = $this->t('Machine name');
    $header['uid'] = $this->t('Owner');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    /** @var \Drupal\user\UserInterface $owner */
    $owner = $entity->getOwner();
    $row['uid'] = $owner ? $owner->toLink() : '';
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->hasLinkTemplate('clone')) {
      $operations['clone'] = [
        'title' => t('Clone'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('clone')),
      ];
    }

    return $operations;
  }

}

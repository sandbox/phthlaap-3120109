<?php

namespace Drupal\test_case_ui;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestCaseManager.
 *
 * @package Drupal\test_case_ui
 */
class TestCaseManager {

  protected $entityTypeManager;

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    AccountInterface $account
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $account;
  }

  /**
   * Return test case select options.
   *
   * @return array
   *   Options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testCaseOptions() {
    $query = $this->entityTypeManager->getStorage('test_case')->getQuery();
    if (!$this->currentUser->hasPermission('administer test case ui configuration')) {
      $filter_owner = $query->orConditionGroup();
      $filter_owner->condition('uid', [$this->currentUser->id(), 0], 'IN');
      $filter_owner->condition('uid', NULL, 'IS NULL');
      $query->condition($filter_owner);
    }
    $result = $query->execute();
    $test_case = $this->entityTypeManager->getStorage('test_case')->loadMultiple($result);
    $output = [];
    foreach ($test_case as $case) {
      $output[$case->id()] = $case->label();
    }
    return $output;
  }

}

<?php

namespace Drupal\test_case_ui;

use Drupal\Component\Serialization\Json;

/**
 * Class TestCaseLog.
 *
 * @package Drupal\test_case_ui
 */
class TestCaseLog {

  protected $lid;

  protected $test_case;

  protected $images;

  protected $message;

  protected $created;

  /**
   * Get Images.
   *
   * @return array|mixed
   *   Images;
   */
  public function getImages() {
    if (empty($this->images)) {
      return [];
    }
    return Json::decode($this->images);
  }

  /**
   * Get message.
   *
   * @return string
   *   Message.
   */
  public function getMessage() {
    return htmlentities($this->message);
  }

  /*
   * Get created time.
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * @return mixed
   */
  public function getId() {
    return $this->lid;
  }

}

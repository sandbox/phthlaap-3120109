<?php

namespace Drupal\test_case_ui\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Traversable;

/**
 * Provides the Case action plugin plugin manager.
 */
class CaseActionPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new CaseActionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct('Plugin/CaseActionPlugin', $namespaces, $module_handler, 'Drupal\test_case_ui\Plugin\CaseActionPluginInterface', 'Drupal\test_case_ui\Annotation\CaseActionPlugin');

    $this->alterInfo('test_case_ui_case_action_plugin_info');
    $this->setCacheBackend($cache_backend, 'test_case_ui_case_action_plugin_plugins');
  }

  /**
   * Get all plugin.
   *
   * @return array
   *   Return options.
   */
  public function getPluginSelectOptions() {
    $plugin_definitions = $this->getDefinitions();
    $options = [];
    foreach ($plugin_definitions as $key => $plugin) {
      $options[$key] = $plugin['label'];
    }
    ksort($options);
    return $options;
  }

}

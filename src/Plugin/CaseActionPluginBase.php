<?php

namespace Drupal\test_case_ui\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * Base class for Case action plugin plugins.
 */
abstract class CaseActionPluginBase extends PluginBase implements CaseActionPluginInterface {

  /**
   * Build form element.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array|null $args
   *   Arguments.
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    $form['case_action_child_form'] = [
      '#type' => 'container',
      '#title' => 'Parameters',
      '#weight' => 9,
    ];
  }

  /**
   * Plugin process.
   *
   * @param \Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface $testCase
   *   Test case.
   * @param array $args
   *   Arguments.
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {

  }

  /**
   * Pre-process value.
   *
   * @param mixed $value
   *   Value to process.
   *
   * @return mixed
   *   Value pre-process.
   */
  public function preProcessArguments($value) {
    return $value;
  }

}

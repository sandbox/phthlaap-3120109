<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * Drupal get action.
 *
 * @CaseActionPlugin(
 *   id = "drupal_get_action",
 *   label = "Drupal get"
 * )
 */
class DrupalGetAction extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Url',
      '#name' => 'args[url]',
      '#default_value' => $args['url'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    $testCase->drupalGet($args['url'], ['external' => TRUE]);
  }

}

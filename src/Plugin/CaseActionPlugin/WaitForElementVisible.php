<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * ElementClick.
 *
 * @CaseActionPlugin(
 *   id = "wait_for_element_visible",
 *   label = "Wait for element visible"
 * )
 */
class WaitForElementVisible extends ElementExists {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'select',
      '#title' => 'Next action',
      '#name' => 'args[next_action]',
      '#options' => [
        'click' => 'click',
        'setValue' => 'setValue',
        'selectOption' => 'selectOption',
        'focus' => 'focus',
      ],
      '#default_value' => $args['next_action'],
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Argument',
      '#name' => 'args[argument]',
      '#default_value' => $args['argument'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $session */
    $session = $testCase->assertSession();
    $action = $args['next_action'];
    $element = $session->waitForElementVisible('css', $args['selector']);
    if (empty($args['argument'])) {
      $element->$action();
    }
    else {
      $element->$action($args['argument']);
    }
  }

}

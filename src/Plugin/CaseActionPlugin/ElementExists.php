<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * Drupal get action.
 *
 * @CaseActionPlugin(
 *   id = "element_exist",
 *   label = "Element Exists"
 * )
 */
class ElementExists extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Selector',
      '#name' => 'args[selector]',
      '#default_value' => $args['selector'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $session */
    $session = $testCase->assertSession();
    $session->elementExists('css', $args['selector']);
  }

}

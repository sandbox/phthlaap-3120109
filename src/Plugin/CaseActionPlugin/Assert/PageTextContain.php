<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin\Assert;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * CaptureScreenShot.
 *
 * @CaseActionPlugin(
 *   id = "page_text_contain",
 *   label = "Page text contain"
 * )
 */
class PageTextContain extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'textarea',
      '#title' => 'Text',
      '#name' => 'args[text]',
      '#default_value' => $args['text'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    // $testCase->captureScreenshot();
    $session = $testCase->assertSession();
    $session->pageTextContains($args['text']);
  }

}

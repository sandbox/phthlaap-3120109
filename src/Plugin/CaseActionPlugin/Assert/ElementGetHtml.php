<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin\Assert;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * ElementGetHtml.
 *
 * @CaseActionPlugin(
 *   id = "element_get_html",
 *   label = "Element get html"
 * )
 */
class ElementGetHtml extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Selector',
      '#name' => 'args[selector]',
      '#default_value' => $args['selector'],
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'checkbox',
      '#title' => 'Dump',
      '#name' => 'args[dump]',
      '#default_value' => !empty($args['dump']) ? $args['dump'] : 0,
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'select',
      '#options' => [
        '_none' => 'None',
        'assertContains' => 'assertContains',
        'assertNotContains' => 'assertNotContains',
        'assertNotEmpty' => 'assertNotEmpty',
      ],
      '#title' => 'Assert',
      '#name' => 'args[assert]',
      '#default_value' => !empty($args['assert']) ? $args['assert'] : '_none',
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Assert text',
      '#name' => 'args[text]',
      '#default_value' => !empty($args['text']) ? $args['text'] : '',
      '#states' => [
        'invisible' => [
          ':input[name="args[assert]"]' => ['value' => '_none'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $session */
    $session = $testCase->assertSession();
    $element = $session->elementExists('css', $args['selector']);
    $value = $element->getHtml();
    if (!empty($args['dump']) && TRUE == boolval($args['dump'])) {
      dump(htmlentities($value));
    }
    if ('_none' != $args['assert'] && !empty($args['text'])) {
      $assert_function = $args['assert'];
      switch ($assert_function) {
        case 'assertNotEmpty':
          $testCase->$assert_function($value);
          break;

        case 'assertContains':
        case 'assertNotContains':
          $testCase->$assert_function($args['text'], $value);
          break;
      }
    }
  }

}

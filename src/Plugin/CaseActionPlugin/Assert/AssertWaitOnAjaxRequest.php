<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin\Assert;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * CaptureScreenShot.
 *
 * @CaseActionPlugin(
 *   id = "assert_wait_for_ajax_request",
 *   label = "Drupal assert wait for ajax request"
 * )
 */
class AssertWaitOnAjaxRequest extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#markup' => '<b>This action for Drupal website only.</b>',
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'number',
      '#title' => 'Timeout',
      '#name' => 'args[timeout]',
      '#suffix' => 'milliseconds',
      '#default_value' => !empty($args['timeout']) ? $args['timeout'] : '',
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Message',
      '#name' => 'args[message]',
      '#default_value' => !empty($args['message']) ? $args['message'] : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    $session = $testCase->assertSession();
    $session->assertWaitOnAjaxRequest($args['timeout'], $args['message']);
  }

}

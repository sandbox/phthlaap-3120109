<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin\Assert;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * Wait.
 *
 * @CaseActionPlugin(
 *   id = "wait",
 *   label = "Wait"
 * )
 */
class Wait extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'number',
      '#title' => 'Timeout',
      '#name' => 'args[timeout]',
      '#suffix' => 'milliseconds',
      '#default_value' => !empty($args['timeout']) ? $args['timeout'] : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    $testCase->getDriverInstance()->wait($args['timeout'], 'false');
  }

}

<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * CaptureScreenShot.
 *
 * @CaseActionPlugin(
 *   id = "capture_screen_shot",
 *   label = "Capture screen shot"
 * )
 */
class CaptureScreenShot extends CaseActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    $testCase->captureScreenshot();
  }

}

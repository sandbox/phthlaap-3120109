<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * ElementSetRandomValue.
 *
 * @CaseActionPlugin(
 *   id = "element_set_random_value",
 *   label = "Element set random value"
 * )
 */
class ElementSetRandomValue extends ElementExists {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'select',
      '#title' => 'Function',
      '#name' => 'args[function]',
      '#options' => [
        'word' => 'word',
        'paragraphs' => 'paragraphs',
        'randomMachineName' => 'randomMachineName',
        'randomNumber' => 'randomNumber',
      ],
      '#default_value' => $args['function'],
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Length',
      '#name' => 'args[length]',
      '#default_value' => $args['length'] ? $args['length'] : 8,
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Prefix',
      '#name' => 'args[prefix]',
      '#default_value' => !empty($args['prefix']) ? $args['prefix'] : '',
    ];
    $form['case_action_child_form'][] = [
      '#type' => 'textfield',
      '#title' => 'Suffix',
      '#name' => 'args[suffix]',
      '#default_value' => !empty($args['suffix']) ? $args['suffix'] : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $session */
    $session = $testCase->assertSession();
    $function = $args['function'];
    $length = $args['length'];
    if (method_exists($testCase, $function)) {
      $value = $testCase->$function($length);
    }
    else {
      $random_generator = $testCase->getRandomGenerator();
      $value = $random_generator->$function($length);
    }
    if (!empty($args['prefix'])) {
      $value = $args['prefix'] . $value;
    }
    if (!empty($args['suffix'])) {
      $value = $value . $args['suffix'];
    }
    $session->waitForElementVisible('css', $args['selector'])->setValue($value);
  }

}

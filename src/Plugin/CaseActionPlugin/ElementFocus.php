<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * ElementFocus.
 *
 * @CaseActionPlugin(
 *   id = "element_focus",
 *   label = "Element focus"
 * )
 */
class ElementFocus extends ElementExists {

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $session */
    $session = $testCase->assertSession();
    $session->waitForElementVisible('css', $args['selector'])->focus();
  }

}

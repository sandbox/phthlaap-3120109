<?php

namespace Drupal\test_case_ui\Plugin\CaseActionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\test_case_ui\Plugin\CaseActionPluginBase;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * ElementSetPassword.
 *
 * @CaseActionPlugin(
 *   id = "element_set_password",
 *   label = "Element set password"
 * )
 */
class ElementSetPassword extends ElementExists {

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL) {
    parent::buildFormElement($form, $form_state, $args);
    $form['case_action_child_form'][] = [
      '#type' => 'password',
      '#title' => 'Value',
      '#name' => 'args[password]',
      '#default_value' => $args['password'],
      '#description' => 'Old password will not show here',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $session */
    $session = $testCase->assertSession();
    $session->waitForElementVisible('css', $args['selector'])->setValue($args['password']);
  }

  /**
   * {@inheritdoc}
   */
  public function preProcessArguments($value) {
    if (!empty($value['password'])) {
      $value['password'] = str_pad('', strlen($value['password']), '*');
    }
    return $value;
  }

}

<?php

namespace Drupal\test_case_ui\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface;

/**
 * Defines an interface for Case action plugin plugins.
 */
interface CaseActionPluginInterface extends PluginInspectionInterface {

  /**
   * Build plugin form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal\Core\Form\FormStateInterface.
   * @param array|null $args
   *   Arguments.
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, array $args = NULL);

  /**
   * Process action.
   *
   * @param \Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface $testCase
   *   Drupal\Tests\test_case_ui\FunctionalJavascript\VirtualTestCaseInterface.
   * @param array|null $args
   *   Arguments.
   */
  public function process(VirtualTestCaseInterface $testCase, array $args = NULL);

}

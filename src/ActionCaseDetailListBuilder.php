<?php

namespace Drupal\test_case_ui;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Test case action entities.
 */
class ActionCaseDetailListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'action_case_detail_list_builder';
  }

  /**
   * Drupal\test_case_ui\Entity\TestCaseEntity.
   *
   * @var \Drupal\test_case_ui\Entity\TestCaseEntity
   */
  protected $testCase;

  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    array $arguments,
    PluginManagerInterface $pluginManager
  ) {
    parent::__construct($entity_type, $storage);
    $this->testCase = $arguments['test_case'];
    $this->weightKey = 'weight';
    $this->pluginManager = $pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type, array $arguments = NULL) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $arguments,
      $container->get('plugin.manager.case_action_plugin')
    );
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery();
    if (!empty($this->testCase)) {
      $query->condition('test_case', $this->testCase->id());
    }
    $query->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Title');
    $header['case_id'] = $this->t('Action label');
    $header['arguments'] = $this->t('Arguments');
    if (!empty($this->weightKey)) {
      $header['weight'] = t('Weight');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();


    /** @var \Drupal\test_case_ui\Plugin\CaseActionPluginInterface $plugin */
    $plugin = $this->pluginManager->createInstance($entity->getCaseAction());
    $definition = $plugin->getPluginDefinition();
    $row['case_id']['data'] = ['#markup' => $definition['label']];
    $arguments = $entity->getArguments();
    $arguments = $plugin->preProcessArguments($arguments);
    $arguments_output = '';
    if (!empty($arguments)) {
      foreach ($arguments as $key => $value) {
        $arguments_output .= $key . ': ' . $value . '<br/>';
      }
    }
    $row['arguments']['data'] = [
      '#markup' => $arguments_output,
    ];
    if (!empty($this->weightKey)) {

      // Override default values to markup elements.
      $row['#attributes']['class'][] = 'draggable';
      $row['#weight'] = $entity
        ->get($this->weightKey);

      // Add weight column.
      $row['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight for @title', [
          '@title' => $entity
            ->label(),
        ]),
        '#title_display' => 'invisible',
        '#default_value' => $entity
          ->get($this->weightKey),
        '#attributes' => [
          'class' => [
            'weight',
          ],
        ],
      ];
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $form = NULL, FormStateInterface $formState = NULL) {
    if (!empty($this->weightKey)) {
      return $this->buildForm($form, $formState);
      //return $this->formBuilder()->getForm($this);
    }
    return parent::render();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);
      if (isset($row['label'])) {
        $row['label'] = ['#markup' => $row['label']];
      }
      if (isset($row['weight'])) {
        $row['weight']['#delta'] = $delta;
      }
      $form[$this->entitiesKey][$entity->id()] = $row;
    }

    // $form['actions']['#type'] = 'actions';
    /*$form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];*/

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ensureDestination(Url $url) {
    $redirect = Url::fromRoute('entity.test_case.edit_form', ['test_case' => $this->testCase->id()]);
    $destination = [
      'destination' => $redirect->toString(),
    ];
    return $url->mergeOptions(['query' => $destination]);
  }

}
